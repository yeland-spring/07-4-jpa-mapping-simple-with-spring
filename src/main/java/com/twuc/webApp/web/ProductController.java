package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @PostMapping("products")
    ResponseEntity<Void> createProducts(@RequestBody @Valid CreateProductRequest product) {
        Product newProduct = productRepository.saveAndFlush(new Product(product.getName(), product.getPrice(), product.getUnit()));
        return ResponseEntity.status(201)
                .header("location", String.format("http://localhost/api/products/%d", newProduct.getId()))
                .build();
    }

    @GetMapping("products/{productId}")
    ResponseEntity<GetProductResponse> getProduct(@PathVariable Long productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        if (!productOptional.isPresent()) {
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.ok(new GetProductResponse(productOptional.get()));
    }
}